import 'package:flutter/material.dart';
import 'my_flutter_app_icons.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Color mainColor = Color(0xff8CA5FF);
    Color secondColor = Color(0xff383C4A);
    return MaterialApp(
      title: 'Card Name',
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: SafeArea(
          child: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topRight,
                end: Alignment.bottomLeft,
                colors: [mainColor, secondColor],
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                CircleAvatar(
                  radius: 60,
                  backgroundColor: Colors.white,
                  child: CircleAvatar(
                    radius: 57,
                    backgroundImage: AssetImage(
                      'images/Profile.jpg',
                    ),
                  ),
                ),
                SizedBox(height: 20),
                Text(
                  'Berri Primaputra',
                  style: TextStyle(
                    fontFamily: 'Righteous',
                    color: Colors.white,
                    fontSize: 35,
                  ),
                ),
                SizedBox(height: 5),
                Text(
                  'FLUTTER DEVELOPER',
                  style: TextStyle(
                      fontFamily: 'Roboto',
                      color: Colors.white,
                      fontSize: 15,
                      fontWeight: FontWeight.w300,
                      letterSpacing: 3),
                ),
                SizedBox(
                  height: 30,
                  width: 150,
                  child: Divider(
                    color: Colors.white,
                  ),
                ),
                Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)
                  ),
                  margin: EdgeInsets.fromLTRB(40, 9, 40, 10),
                  child: ListTile(
                    leading: Icon(
                      MyFlutterApp.phone_call,
                      color: secondColor,
                      size: 27,
                    ),
                    title: Text(
                      '0812-5612-3432',
                      style: TextStyle(
                        fontFamily: 'Roboto',
                        fontSize: 18,
                        letterSpacing: 1.7,
                        color: secondColor,
                        fontWeight: FontWeight.w900,
                      ),
                    ),
                  ),
                ),
                Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  margin: EdgeInsets.fromLTRB(40, 10, 40, 10),
                  child: ListTile(
                    leading: Icon(
                      MyFlutterApp.mail,
                      color: secondColor,
                      size: 27,
                    ),
                    title: Text(
                      'berri.primaputra@gmail.com',
                      style: TextStyle(
                        fontFamily: 'Roboto',
                        fontSize: 18,
                        color: secondColor,
                        fontWeight: FontWeight.w900,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
